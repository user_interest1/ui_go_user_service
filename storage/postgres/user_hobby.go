package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"user_interest/ui_go_user_service/genproto/user_service"
	"user_interest/ui_go_user_service/pkg/helper"
	"user_interest/ui_go_user_service/storage"
)

type UserHobbyRepo struct {
	db *pgxpool.Pool
}

func NewUserHobbyRepo(db *pgxpool.Pool) storage.UserHobbyRepoI {
	return &UserHobbyRepo{
		db: db,
	}
}

func (c *UserHobbyRepo) Create(ctx context.Context, req *user_service.CreateUserHobby) (resp *user_service.UserHobbyPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "hobbies" (
				id,
				name,
				updated_at
			) VALUES ($1, $2, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserHobbyPrimaryKey{Id: id.String()}, nil
}

func (c *UserHobbyRepo) GetByPKey(ctx context.Context, req *user_service.UserHobbyPrimaryKey) (resp *user_service.UserHobby, err error) {

	query := `
		SELECT
			id,
			name,
			created_at,
			updated_at
		FROM "hobbies"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.UserHobby{
		Id:        id.String,
		Name:      name.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *UserHobbyRepo) GetAll(ctx context.Context, req *user_service.GetListUserHobbyRequest) (resp *user_service.GetListUserHobbyResponse, err error) {

	resp = &user_service.GetListUserHobbyResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "hobbies"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.UserHobbies = append(resp.UserHobbies, &user_service.UserHobby{
			Id:        id.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *UserHobbyRepo) Update(ctx context.Context, req *user_service.UpdateUserHobby) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "hobbies"
			SET
				name = :name,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":   req.GetId(),
		"name": req.GetName(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *UserHobbyRepo) Delete(ctx context.Context, req *user_service.UserHobbyPrimaryKey) error {

	query := `DELETE FROM "hobbies" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
