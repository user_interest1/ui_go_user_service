package service

import (
	"context"

	"user_interest/ui_go_user_service/genproto/user_service"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"user_interest/ui_go_user_service/config"
	"user_interest/ui_go_user_service/grpc/client"
	"user_interest/ui_go_user_service/pkg/logger"
	"user_interest/ui_go_user_service/storage"
)

type UserHobby struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedUserHobbyServiceServer
}

func NewUserHobby(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *UserHobby {
	return &UserHobby{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *UserHobby) Create(ctx context.Context, req *user_service.CreateUserHobby) (resp *user_service.UserHobby, err error) {

	i.log.Info("---CreateUserHobby------>", logger.Any("req", req))

	pKey, err := i.strg.UserHobby().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateUserHobby->UserHobby->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.UserHobby().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyUserHobby->UserHobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserHobby) GetByID(ctx context.Context, req *user_service.UserHobbyPrimaryKey) (resp *user_service.UserHobby, err error) {

	i.log.Info("---GetUserHobbyByID------>", logger.Any("req", req))
	resp, err = i.strg.UserHobby().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserHobbyByID->UserHobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserHobby) GetList(ctx context.Context, req *user_service.GetListUserHobbyRequest) (resp *user_service.GetListUserHobbyResponse, err error) {

	i.log.Info("---GetUserHobbyies------>", logger.Any("req", req))
	resp, err = i.strg.UserHobby().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserHobbys->UserHobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserHobby) Update(ctx context.Context, req *user_service.UpdateUserHobby) (resp *user_service.UserHobby, err error) {

	i.log.Info("---UpdateUserHobby------>", logger.Any("req", req))

	rowsAffected, err := i.strg.UserHobby().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateUserHobby--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.UserHobby().GetByPKey(ctx, &user_service.UserHobbyPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUserHobby->UserHobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *UserHobby) Delete(ctx context.Context, req *user_service.UserHobbyPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteUserHobby------>", logger.Any("req", req))

	err = i.strg.UserHobby().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteUserHobby->UserHobby->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
